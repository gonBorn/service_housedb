-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION';

-- -----------------------------------------------------
-- Schema mydb
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema mydb
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `mydb` DEFAULT CHARACTER SET utf8 ;
USE `mydb` ;

-- -----------------------------------------------------
-- Table `mydb`.`client`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`client` (
  `client_id` INT NOT NULL AUTO_INCREMENT,
  `full_name` VARCHAR(128) NOT NULL,
  `abbreviation` VARCHAR(6) NOT NULL,
  PRIMARY KEY (`client_id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mydb`.`service`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`service` (
  `service_id` INT NOT NULL AUTO_INCREMENT,
  `service_type` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`service_id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mydb`.`contract`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`contract` (
  `contract_id` INT NOT NULL AUTO_INCREMENT,
  `contract_name` VARCHAR(45) NOT NULL,
  `client_id` INT NOT NULL,
  `service_service_id` INT NOT NULL,
  PRIMARY KEY (`contract_id`, `client_id`, `service_service_id`),
  INDEX `fk_contract_client_idx` (`client_id` ASC) VISIBLE,
  INDEX `fk_contract_service1_idx` (`service_service_id` ASC) VISIBLE,
  CONSTRAINT `fk_contract_client`
    FOREIGN KEY (`client_id`)
    REFERENCES `mydb`.`client` (`client_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_contract_service1`
    FOREIGN KEY (`service_service_id`)
    REFERENCES `mydb`.`service` (`service_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mydb`.`office`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`office` (
  `office_id` INT NOT NULL AUTO_INCREMENT,
  `country` VARCHAR(45) NOT NULL,
  `city` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`office_id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mydb`.`stuff`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`stuff` (
  `stuff_id` INT NOT NULL AUTO_INCREMENT,
  `first_name` VARCHAR(45) NOT NULL,
  `last_name` VARCHAR(45) NOT NULL,
  `office_office_id` INT NOT NULL,
  PRIMARY KEY (`stuff_id`, `office_office_id`),
  INDEX `fk_stuff_office1_idx` (`office_office_id` ASC) VISIBLE,
  CONSTRAINT `fk_stuff_office1`
    FOREIGN KEY (`office_office_id`)
    REFERENCES `mydb`.`office` (`office_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mydb`.`contract_service_office`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`contract_service_office` (
  `contract_contract_id` INT NOT NULL,
  `office_office_id` INT NOT NULL,
  `stuff_stuff_id` INT NOT NULL,
  PRIMARY KEY (`contract_contract_id`, `office_office_id`, `stuff_stuff_id`),
  INDEX `fk_contract_has_office_office1_idx` (`office_office_id` ASC) VISIBLE,
  INDEX `fk_contract_has_office_contract1_idx` (`contract_contract_id` ASC) VISIBLE,
  INDEX `fk_contract_service_office_stuff1_idx` (`stuff_stuff_id` ASC) VISIBLE,
  CONSTRAINT `fk_contract_has_office_contract1`
    FOREIGN KEY (`contract_contract_id`)
    REFERENCES `mydb`.`contract` (`contract_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_contract_has_office_office1`
    FOREIGN KEY (`office_office_id`)
    REFERENCES `mydb`.`office` (`office_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_contract_service_office_stuff1`
    FOREIGN KEY (`stuff_stuff_id`)
    REFERENCES `mydb`.`stuff` (`stuff_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
